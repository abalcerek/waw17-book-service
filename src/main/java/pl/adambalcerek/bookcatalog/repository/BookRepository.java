package pl.adambalcerek.bookcatalog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.adambalcerek.bookcatalog.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
}
