package pl.adambalcerek.bookcatalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.adambalcerek.bookcatalog.model.Book;
import pl.adambalcerek.bookcatalog.service.BookService;

@Controller
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(@Qualifier("DBBookService")
                                      BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("forward:/book-list");
    }

    @GetMapping("book-list")
    public ModelAndView books() {
        ModelAndView model = new ModelAndView("book-list");
        model.addObject("books", bookService.getBooks());
        return model;
    }

    @GetMapping("book-detail")
    public ModelAndView bookDetail(@RequestParam("id") Long id) {
        ModelAndView model = new ModelAndView("book-detail");
        model.addObject("book", bookService.getBook(id));
        return model;
    }

    @PostMapping("delete-book")
    public ModelAndView deleteBook(@ModelAttribute Book book) {
        System.out.println("book: " + book);
        bookService.removeBook(book.getId());

        ModelAndView model
                = new ModelAndView("redirect:/book-list");
        return model;
    }

    @GetMapping("book-form")
    public ModelAndView bookForm() {
        ModelAndView modelAndView
                = new ModelAndView("book-form");
        modelAndView.addObject("book", new Book());
        return modelAndView;
    }

    @PostMapping("save-book")
    public ModelAndView saveBook(@ModelAttribute Book book) {
        bookService.save(book);
        return new ModelAndView("redirect:/book-list");
    }

}
