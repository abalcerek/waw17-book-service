package pl.adambalcerek.bookcatalog.service;

import pl.adambalcerek.bookcatalog.model.Book;

import java.util.List;

public interface BookService {

    List<Book> getBooks();

    Book getBook(Long id);

    void removeBook(Long id);

    void save(Book book);
}
