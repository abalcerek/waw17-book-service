package pl.adambalcerek.bookcatalog.service.impl;

import org.springframework.stereotype.Service;
import pl.adambalcerek.bookcatalog.model.Book;
import pl.adambalcerek.bookcatalog.repository.BookRepository;
import pl.adambalcerek.bookcatalog.service.BookService;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
class DBBookService implements BookService {

    private BookRepository bookRepository;

    DBBookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @PostConstruct
    public void init() {
        //todo: create list of books

        //todo: put books into book table with bookRepository
        bookRepository.save(new Book(null,
                "Python dla każdego. Podstawy programowania",
                "45234523",
                "Chcesz się nauczyć programować? Świetna decyzja! Wybierz język obiektowy, łatwy w użyciu, " +
                        "z przejrzystą składnią. Python będzie wprost doskonały! Rozwijany od ponad 20 lat, " +
                        "jest dojrzałym językiem, pozwalającym tworzyć zaawansowane aplikacje dla różnych systemów " +
                        "operacyjnych. Ponadto posiada system automatycznego zarządzania pamięcią, który zdejmuje " +
                        "z programisty obowiązek panowania nad tym skomplikowanym obszarem.",
                "Dawson Michael",
                "2014"));

    }


    @Override
    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book getBook(Long id) {
        return null;
    }

    @Override
    public void removeBook(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }
}
